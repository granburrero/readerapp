<?php
require_once __DIR__.'/../vendor/autoload.php';
use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__.'/..');
$dotenv->load();

return [
    //Interface => Implementation
    Burrero\Shared\Domain\FileLocator::class            => DI\get(Burrero\Shared\Infrastructure\PhpFileLocator::class),
    Burrero\Shared\Domain\Uuid::class                   => DI\get(Burrero\Shared\Infrastructure\Uuid4\UuidApp::class),
    \Burrero\Shared\Domain\Messenger::class             => DI\get(\Burrero\Shared\Infrastructure\Messenger\PhpRabbittMQMessageBus::class),
    Burrero\Shared\Domain\Core\Policy::class            =>DI\get(Burrero\Shared\Infrastructure\Core\ApplicationPolicy::class),

    //Services
    \Psr\Log\LoggerInterface::class                     => \DI\Factory(function () {
        $logger = new Monolog\Logger('Kernel-Log-Service');

        $fileHandler = new Monolog\Handler\StreamHandler(__DIR__.'/../var/logs/app.log', Monolog\Logger::DEBUG);
        $fileHandler->setFormatter(new Monolog\Formatter\LineFormatter());
        $logger->pushHandler($fileHandler);

        return $logger;
    }),
    \Doctrine\ORM\EntityManagerInterface::class         => \DI\Factory(function () {
        $isDevMode = false;
        $paths = array(__DIR__.'/../conf/xml');

        $dbParams = array(
            'driver'   => 'pdo_mysql',
            'host'     => $_ENV['MYSQL_SERVER'],
            'user'     => $_ENV['MYSQL_USER'],
            'password' => $_ENV['MYSQL_PASS'],
            'dbname'   => $_ENV['MYSQL_DBNAME']
        );

        $config = \Doctrine\ORM\Tools\Setup::createXMLMetadataConfiguration($paths, $isDevMode);
        $entityManager = \Doctrine\ORM\EntityManager::create($dbParams, $config);

        return $entityManager;
    })
];