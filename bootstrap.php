<?php 
require_once __DIR__.'/vendor/autoload.php';
use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

 $isDevMode = false;
        $paths = array(__DIR__.'/config/xml');

        $dbParams = array(
            'driver'   => 'pdo_mysql',
            'host'     => $_ENV['MYSQL_SERVER'],
            'user'     => $_ENV['MYSQL_USER'],
            'password' => $_ENV['MYSQL_PASS'],
            'dbname'   => $_ENV['MYSQL_DBNAME']
        );
        
$config = \Doctrine\ORM\Tools\Setup::createXMLMetadataConfiguration($paths, $isDevMode);
$entityManager = \Doctrine\ORM\EntityManager::create($dbParams, $config);