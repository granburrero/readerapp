<?php declare(strict_types=1);
namespace Burrero\Backoffice\Application\DataTransform;

require __DIR__.'/../../../../kernel.php';

use PhpOffice\PhpSpreadsheet\IOFactory;


class PhpXlsxFileRead
{
    function __invoke($client, $app)
    {
        $locate = new PhpXlsxFileLocate(
            $this->container->get('Burrero\\Shared\\Infrastructure\\PhpFileLocator'),
            $this->container->get('Psr\\Log\\LoggerInterface'),
            $this->container->get('Burrero\\Shared\\Infrastructure\\Uuid4\\UuidApp')
        );
        $inputFileName = '/../../../../repository/*.xlsx';

        $arrayInputFileName = $locate->doLocate($client, __DIR__. $inputFileName, $app);
        try {
            foreach ($arrayInputFileName as $key => $value) {
                $inputFiletype = IOFactory::identify($value);
                $reader = IOFactory::createReader($inputFiletype);
        
                $spreadsheet = $reader->load($value);
                $cellValue = $spreadsheet->getActiveSheet()->toArray(null, null, true, true);
        
                file_put_contents(__DIR__ .'/../../../../dump/'.$key . '.json', json_encode($cellValue));
                
                return $arrayInputFileName;
            }
        } catch (\Throwable $th) {
            $locate->errorHandler($th->getMessage());
            return false;
        }
    }
    
}
