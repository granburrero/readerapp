<?php declare(strict_types=1);
namespace Burrero\Backoffice\Application\DataTransform;

use Burrero\Shared\Domain\FileLocator;
use Burrero\Shared\Domain\Uuid;
use Psr\Log\LoggerInterface;

final class PhpXlsxFileLocate
{
    public function __construct(
        private FileLocator $fileLocator, 
        private LoggerInterface $loggerInterface, 
        private Uuid $uuid
        )
    {
    }

    /**
     * runs the xlsx file location
     */
    public function doLocate(string $client, string $dir, string $excution = null): mixed
    {
        $array = $this->fileLocator->locationToArray($dir);
        $eventId = $this->uuid->uuidCreate();
        $target = explode('*.', $dir);
        
        if (!empty($array)) {
            $this->loggerInterface->info(get_class() . '::class',[
                'id'        => $eventId,
                'Method'    => __METHOD__,
                'Throw'     => 'TouchDown!!!💞'
            ]);

            $payload = [
                'aggregate_id'      => $eventId,
                'name'              => $client. "@" . __METHOD__ . "@" . $target[1] . "@" . $excution,
                'body'              => $array
            ];

            //$this->messenger->dispatch('domain-event', json_encode($payload));
            
            //$this->messenger->dispatch('container-main-work', $eventId);

            return $payload;

        } else {
            $this->loggerInterface->critical(get_class() . '::class',[
                'id'        => $eventId,
                'Method'    => __METHOD__,
                'Throw'     => 'Location is empty 💔'
                ]);
            return false;
        }
    }

    public function errorHandler($th): void
    {
        $this->loggerInterface->critical(get_class() . '::class',[
            'id'        => $this->uuid->uuidCreate(),
            'Method'    => __METHOD__,
            'Throw'     => $th . ' 💔'
            ]);
    }
}
