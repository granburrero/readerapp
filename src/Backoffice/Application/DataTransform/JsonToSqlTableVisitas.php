<?php declare(strict_types = 1);

namespace Burrero\Backoffice\Application\DataTransform;

require_once __DIR__.'/../../../../libs/Spyc/Spyc.php';

use Burrero\Shared\Infrastructure\Utils\BaseApplication;
use RuntimeException;

final class JsonToSqlTableVisitas extends BaseApplication
{

  public function __invoke(string $jsonName, string $staleName): mixed
  {
  //msg es el nombre de archivo json a convertir a sql
    try {
      $jsonMsg = file_get_contents(__DIR__. '/../../../../dump/' .$jsonName.'.json');
      $arrayMsg = json_decode($jsonMsg, true);

      $count = count($arrayMsg);
      //var_dump($count);
      $dto = spyc_load_file(__DIR__ . '/../../../../main_conf/template_visitas.yaml');
      $end = parent::endKey($dto);
      $usable = [];
      $cells = [];

      $sqlDump = "/* StaleName\r\n";
      $sqlDump .= "* ". $staleName . "\r\n";
      $sqlDump .= "StaleName */ \r\n";
      $sqlDump .= "\r\n";

      $sqlDump .= "CREATE TABLE IF NOT EXISTS `visitas` (\r\n";
      $sqlDump .= "`id` INT(11) NOT NULL AUTO_INCREMENT,\r\n";

      foreach ($dto as $key => $value) {
        //$key es letra
        //$value[0]['usable'] = true
        //$value[1]['transform'] = false
        //$value[2]['class'] = array
        //$value[3]['header'] = srting
        //$value[4]['sql'] = string
        if ($value[0]['usable'] == true) {
          
          $sqlDump .= $value[4]['sql']."\r\n";
          $cell = explode('` ',$value[4]['sql']);
          array_push($cells,$cell[0]);
          array_push($usable,$key);
        }
      }
      $sqlDump .="PRIMARY KEY (`id`)\r\n";
      $sqlDump .=") ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;\r\n";

      for ($i=1; $i < $count ; $i++) { 
        if ($i == 1) {
          # code...
        } else {
          $sqlDump .="INSERT INTO `visitas` (";

          foreach ($cells as $cellValue) {
            $cellValue == '`observaciones'? $sqlDump .= $cellValue."`": $sqlDump .=$cellValue."`, ";
          }

          $sqlDump .=") VALUES (";
          //$sqlDump .= $i-1 .", ";
          foreach ($usable as $value) {

            $target = $arrayMsg[$i][$value];

            if ($value == "A") {
              $class = parent::loadClass($dto['A'][2]['class'][0]);
              $classExecute = new $class;
              $target = call_user_func($classExecute,$target);
            }

            if ($target == null) {
              $target = "NULL";
            } else { 
              $target ='"'.$target.'"';
            }

            if ($value != $end) {
              $sqlDump .=  $target. ", ";
            } else {
              $sqlDump .= $target;
            }
            
          }

          $sqlDump .=");\r\n";

        }
      }

      file_put_contents(__DIR__. '/../../../../dump/' . $jsonName . '.sql',$sqlDump);
      //finalizamos con reporte
      self::disableStaleName($staleName);
      return true;
    } catch (RuntimeException $th) {
      //throw $th;
      return $th->getMessage();
    }
  }

  /**
   * Mark to erase - prepare to erase source file
   * @return void
   */
  private static function disableStaleName(string $name): void
  {
    $name = explode('repository/',$name);
    $nameToSearch = __DIR__. '/../../../../repository/' . $name[1];
    $nameToRename = __DIR__. '/../../../../repository/' . $name[1] . '.toerase';

    rename($nameToSearch,$nameToRename);
  }

}