<?php declare(strict_types=1);

namespace Burrero\Shared\Domain;

interface FileLocator
{
    public function locationToArray($dir): array|false;
}