<?php declare(strict_types=1);

namespace Burrero\Shared\Domain;

interface Messenger
{
    public function dispatch($channelApp, $messageApp): void;
}