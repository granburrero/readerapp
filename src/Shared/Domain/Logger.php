<?php declare(strict_types = 1);

namespace Burrero\Shared\Domain;

interface Logger
{
    /**
     * 
     * @param string $app application level issue
     * @param string $message
     * @param array $context
     * 
     * @return self
     */
    
    public function catchString(string $app, string $message, array $context = []): self;

    /**
     *
     * @param integer $mode <0|null .json>
     *                      <1 AMQP>
     * @return void
     */
    public function sendCatchString(int|null $mode = null): void;
}