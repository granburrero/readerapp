<?php declare(strict_types=1);
namespace Burrero\Shared\Domain\Core;

interface Policy
{
    public function appCost(int $appSize, int $plan, int $storage, int $unit):float;
    /**
     * Validate if ops is affordable
     * @param float $balance User balance
     * @param null|float $bill to execute
     * @param float $exCode Promotion code
     * @throw Exception 12 this execution is not affordable
     */
    public function itsAffordable(float $balance, ?float $bill, ?float $exCode):void;
    public function appRef(string $params):mixed;
}