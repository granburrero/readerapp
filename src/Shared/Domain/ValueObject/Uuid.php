<?php declare(strict_types=1);

namespace Burrero\Shared\Domain\ValueObject;

use Ramsey\Uuid\Uuid as RamseyUuid;

class Uuid
{

    public static function random(): string
    {
        return RamseyUuid::uuid4()->toString();
    }
}
