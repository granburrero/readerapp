<?php declare(strict_types = 1);

namespace Burrero\Shared\Domain;

interface Uuid
{
    public function uuidCreate(): string;
}