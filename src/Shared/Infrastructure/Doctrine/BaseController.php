<?php declare(strict_types=1);
namespace Burrero\Shared\Infrastructure\Doctrine;

use Burrero\Shared\Infrastructure\Utils\BaseApplication;
use Burrero\Shared\Infrastructure\SecureDataControl\DataConst;
use Burrero\Shared\Infrastructure\SecureDataControl\CreateSecureDataControl;
use Doctrine\ORM\EntityManagerInterface;
use Exception;

class BaseController extends BaseApplication
{
    function __construct(private EntityManagerInterface $entityManager){}

    /**
     * Load client data from client id.
     * If client Cache is enable load fron file instead BD.
     * @param string $clientUUID is a Valid UUID V4 type
     * @param bool $enableCache = true Use data cache
     * @return self
     * @category Data Handle
     * @access public
     * @throw Exception 874 failed to load file data cache
     */
    public function loadClient(string $clientUUID, bool $enableCache = true): self
    {
        $conn = $this->entityManager->getConnection();
        if ($enableCache == true) {
            if (DataConst::checkSDCFile($clientUUID, '/clientCache.php')) {
                include (__DIR__.DataConst::SDC_LOCATION.$clientUUID.'/clientCache.php');
        
                $this->client = $dataCache->client;
                $this->plan = $dataCache->plan;
            } else {
                throw new Exception('Fail to load file ../'.$clientUUID.'/clientCache.php',874);
            }
            
        } else {
            $this->client = $conn->fetchAllAssociative("SELECT * FROM `burrero_clients` WHERE id = ?", (array) $clientUUID);
            $this->plan = $conn->fetchAllAssociative("SELECT * FROM `burrero_app` WHERE client = ?", (array) $clientUUID);

            CreateSecureDataControl::CreateClientCacheSecureDataControl((object) ["client" => $this->client, "plan" => $this->plan], $clientUUID);
            
        }
        $this->account = $conn->fetchAllAssociative("SELECT transaction FROM `burrero_transaction` WHERE client = ? ORDER BY occurred_on", (array) $clientUUID);
        return $this;
    }

    /**
     * Load client consumption plan.
     * @param void
     * @return self
     * @category getter
     * @access public
     */
    public function loadClientPlan():self
    {
        $this->clientPlan = $this->plan;
        return $this;
    }

    /**
     * Get client contracted plan.
     * Minimun plan 5.
     * @param void
     * @return int
     * @category getter
     * @access public
     */ 
    public function getClientPlan():int
    {
        return intval($this->clientPlan[0]['plan']);
    }

    /**
     * Get client contracted storage.
     * Minimun storage 1000000000.
     * @param void
     * @return int
     * @category getter
     * @access public
     */ 
    public function getClientStorage():int
    {
        return intval($this->clientPlan[0]['storage']);
    }

    /**
     * Get client Relative Cost Unit.
     * Minimun RCU 300.
     * @param void
     * @return int
     * @category getter
     * @access public
     */
    public function getClientUnit():int
    {
        return intval($this->clientPlan[0]['unit']);
    }

    /**
     * Load client account from BD.
     * @param void
     * @return self
     * @category Data Handle
     * @access public
     */
    public function loadClientAccount(): self
    {
        $occur = count($this->account);
        for ($i=0; $i < $occur; $i++) { 
            $this->arrayClientAccount[] = json_decode($this->account[$i]['transaction'], true);
        }
        return $this;
    }

    /**
     * Obtains the customer's balance from an arithmetic operation of debit and credit transactions.
     * At least there must be a credit operation in transactions 
     * @param void
     * @return float
     * @category Data handle
     * @access public
     */
    public function getClientBalance(): float
    {
        foreach ($this->arrayClientAccount as $value) {
            if ($value['transactType'] == 'credit') {
                $sum[] = $value['amount'];
            } else {
                $sum[] = - $value['amount'];
            }
            
        }
        return array_sum($sum);
    }

    /**
     * Get client id.
     * @param void
     * @return string
     * @category getter
     * @access public
     */
    public function getClientId(): string
    {
        return $this->client[0]['id'];
    }

    /**
     * Get client name.
     * @param void
     * @return string
     * @category getter
     * @access public
     */
    public function getClientName(): string
    {
        return $this->client[0]['name'];
    }

    /**
     * Get client status.
     * @param void
     * @return string
     * @category getter
     * @access public
     */
    public function getClientStatus(): string
    {
        return $this->client[0]['status'];
    }

    /**
     * Get client products.
     * @param void
     * @return string JSON FORMATED
     * @category getter
     * @access public
     */
    public function getClientProducts(): string
    {
        return $this->client[0]['products'];
    }

    /**Utilities */

    /**
     * Returns an array of required data from a customer.
     * @param void
     * @return array
     * @category Data handle
     * @access public
     */
    public function clientToArray(): array
    {
        return [
            'id'        => $this->getClientId(),
            'name'      => $this->getClientName(),
            'status'    => $this->getClientStatus(),
            'products'  => $this->getClientProducts(),
            'account'   => $this->loadClientAccount()
        ];
    }

    /**
     * Returns a JSON formated string fron clientToArray().
     * @param void
     * @return string
     * @category Data handle
     * @access public
     */
    public function clientToJson(): string
    {
        return json_encode($this->clientToArray());
    }

    /**
     * Returns an array of required data from a customer Products.
     * @param void
     * @return array
     * @category Data handle
     * @access public
     */
    public function clientProductToArray(): array
    {
        return json_decode($this->getClientProducts(), true);
    }

    /**
     * Throw an Exception if client is not active
     * @param void
     * @return void
     * @category Data Validator
     * @access public
     * @throw Exception 5404 Client is not active
     */
    public function isActive(): void
    {
        if ($this->getClientStatus() != 'active') {
            throw new \Exception('#5404#>Client is not active', 5404);
        }
    }

    /**
     * Throw an Exception if client is not able to use a product or internal feature
     * @param string App UUID V4
     * @return void
     * @category Data Validator
     * @access public
     * @throw Exception 85669 This user is not able to use that app
     */
    public function checkProductToUse(string $key): void
    {
        if (array_key_exists($key, $this->clientProductToArray()) == false) {
            throw new \Exception('#85669#>This user is not able to use that app', 85669);
        }
    }
}
