<?php declare(strict_types=1);

namespace Burrero\Shared\Infrastructure;

use Burrero\Shared\Domain\FileLocator;
use Burrero\Shared\Domain\ValueObject\Uuid;

final class PhpFileLocator implements FileLocator
{
    public function locationToArray($dir): array | false
    {
        
        $array = glob($dir);
        if ($array == false) {
            $arrayReturn = $array;
        } else {
            foreach ($array as $value) {
                $arrayReturn[Uuid::random()] = $value;
    
                array_merge($arrayReturn);
            }
        }
        
        return $arrayReturn;
    }
}
