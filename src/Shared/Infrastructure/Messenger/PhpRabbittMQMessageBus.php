<?php declare(strict_types=1);

namespace Burrero\Shared\Infrastructure\Messenger;

use Burrero\Shared\Domain\Messenger;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

final class PhpRabbittMQMessageBus implements Messenger
{
    public function dispatch($channelApp, $messageApp): void
    {
        $connection = new AMQPStreamConnection($_ENV['RABBIT_HOST'], 5672, $_ENV['RABBIT_USER'], $_ENV['RABBIT_PASS']);
        $channel = $connection->channel();
        $channel->queue_declare($channelApp, false, false, false, false);
        $msg = new AMQPMessage($messageApp);
        $channel->basic_publish($msg,'',$channelApp);
    }
}