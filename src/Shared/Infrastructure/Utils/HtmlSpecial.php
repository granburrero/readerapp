<?php declare(strict_types=1);
namespace Burrero\Shared\Infrastructure\Utils;

class HtmlSpecial
{
    /**
   * Clear html tag
   * @return string
   */
    public function __invoke(string $string): string
    {
        $string = str_replace(['<!--','-->'], '', $string);
        return $string;
    }
}
