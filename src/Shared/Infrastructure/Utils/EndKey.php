<?php declare(strict_types=1);
namespace Burrero\Shared\Infrastructure\Utils;

class EndKey
{
    public function __invoke(array $array): mixed
    {
        end($array);
        return key($array);
    }
}
