<?php declare(strict_types=1);
namespace Burrero\Shared\Infrastructure\Utils;

use Ramsey\Uuid\Uuid;

class StringHandler{

    /**
     * prepare the input parameters to the controller to convert them to a valid JSON string 
     * @param string $string MainController input parameters
     * @param string $modo get from BaseAplication::getTypeTransaction()
     * @return self
     */
    public function paramHandler(string $string, string $modo):self
    {
        //session:455778b1-049d-4b30-a850-d5c64265ee10+ref:589665+credit:10+via:paypal 
        //session:71a37081-87af-4b03-b7da-2a41d6b23802+proId:25*2+proId:31*2+proId:6*1+proId:25*2+proId:31*2+proId:6*1+proId:25*2+proId:31*2+proId:6*1+lat:10.686272202785565+lon:-71.61005115433208
        $this->modo = $modo;
        $this->string = explode('+',$string);
        //string[0] = "session:455778b1-049d-4b30-a850-d5c64265ee10"
        //string[1] = "ref:589665"/proId:25*2
        //string[2] = "credit:10"/proId:31*2
        //string[3] = "via:paypal"/proId:6*1
        $count = count($this->string);
        for ($i=0; $i < $count; $i++) { 
            $getInternal = explode(':',$this->string[$i]);
            if ($this->modo == 'debit') {
                $cant = explode('*',$getInternal[1]);
                if (array_key_exists(1, $cant)) {  
                    $setInternal[$i] = [
                        'simpleID'          => $i + 1,
                        $getInternal[0]     => $cant[0],
                        'amount'            => intVal($cant[1])//:int
                    ];
                } else {
                    $setInternal[$i][$getInternal[0]] = $getInternal[1];
                }
            } else {
                $setInternal[$i][$getInternal[0]] = $getInternal[1];
            }
            //array_merge($setInternal);
        }

        $this->refVal = json_encode($setInternal);
        return $this;
    }

    /**
     * return a valid reference
     * @return string uuid for debit or internal reference for credit
     */
    public function getReference():string
    {
        if ($this->modo == 'debit') {
            $ref = Uuid::uuid4()->toString();
        } else {
            $ref = json_decode($this->refVal, true);
            $ref = $ref[1]['ref'];
        }
        return $ref;
    }
    /**
     * return only credit amount
     * @return float
     */
    public function getAmountCredit():float
    {
        if ($this->modo == 'credit') {
            $ref = json_decode($this->refVal, true);
            $ref = $ref[2]['credit'];
        }
        return intVal($ref);
    }
    /**
     * return all data in json string
     * @return string type JSON
     */
    public function getAllData():string
    {
        return $this->refVal;
    }
}
//$log = new StringHandler;
//$instancia = $log->paramHandler('proId:25*2+proId:31*2+proId:6*1+proId:25*2+proId:31*2+proId:6*1+proId:25*2+proId:31*2+proId:6*1+coord:10.686272202785565,-71.61005115433208+dirRef:calle 87 salvador dali 85-47','debit');
//var_dump($instancia->getAllData(), $instancia->getReference());