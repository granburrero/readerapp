<?php declare(strict_types=1);
namespace Burrero\Shared\Infrastructure\Utils;

use Burrero\Shared\Infrastructure\Uuid4\UuidApp;
use Exception;

require_once __DIR__.'/../../../../libs/Spyc/Spyc.php';

class BaseApplication
{

  
  /**
   * Load App registered
   * @param string $appUUID is a Valid UUID V4 type
   * @return self
   * @category Data Handle
   * @access public
   * @throw Exception 17 The app is not registered
   */
  public function loadApp(string $appUuid): self
   {
    $this->dto = spyc_load_file(__DIR__.'/../../../../config/appLib.yaml');
    
    if (!$this->dto[$appUuid]) {
      throw new Exception('#17#> The id ' . $appUuid . ' is not registered',17);
    } else {
      $this->value = $this->dto[$appUuid];
    }
    
    return $this;     
   }

  /**
   * Return a Class-nonqualified full namespace
   * @param void
   * @return string Class-nonqualified full namespace
   * @category Getter
   * @access public
   */
    public function getNameSpace():string
    {
      return $this->value[0]['class'];
    }

  /**
   * Return type transact (debit-credit) signed in app register
   * @param void
   * @return string Type Transaction
   * @category Getter
   * @access public
   */
    public function getTypeTransaction():string
    {
      return $this->value[1]['type'];
    }

  /**
   * Return a Dependency class nonqualified full namespace
   * @param void
   * @return string Dependency class nonqualified full namespace
   * @category Getter
   * @access public
   */
   public function getDependNameSpace():string|null
   {
     return $this->value[2]['depends'];
   }

  /**
   * Transform a string to a fully qualified namespace
   * @param string A class-nonqualified full namespace
   * @return string transform a string to a fully qualified namespace 
   * @category Data Transform
   * @access public
   */
   public static function loadClass(string $class): string
   {
     return str_replace('.','\\',$class);
   }

  /**
   * Return a Class-qualified full namespace
   * @param void
   * @return string Class-qualified full namespace  
   * @category Getter
   * @access public
   */
   public function appNamespace():string
   {
     return $this->loadClass($this->getNameSpace());
   }

  /**
   * Return a Dependency class qualified full namespace
   * @param void
   * @return string Dependency class qualified full namespace    
   * @category Getter
   * @access public
   */
   public function dependNamespace():string
   {
     return $this->loadClass($this->getDependNameSpace());
   }

  /**
   * Validate if App is active
   * @param void
   * @return void
   * @category Validator
   * @access public
   * @throw Exception 75201 this App is not active
   */
   public function appIsActive():void
   {
     if ($this->value[4]['active'] == false) {
       throw new Exception('#75201#> This App is not active', 75201);
     }
   }

  /**
   * Returns if an application is billable 
   * @param void
   * @return bool
   * @category Data Handler
   * @access public
   */
   public function appIsBillable():bool
   {
     return $this->value[3]['billable'];
   }

  /**
   * Returns a string of type UUID V4 
   * @param void
   * @return string UUID type
   * @category Data generator
   * @access public
   */
   public function eventId():string
   {
     $uuid = new UuidApp;
     return $uuid->uuidCreate();
   }

  /**
   * Get the last key of a one-dimensional array 
   * @param array
   * @return mixed
   * @category Data Handler
   * @access public
   */
   public static function endKey(array $array): mixed
   {
     end($array);
     return key($array);
   }

}