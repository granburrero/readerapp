<?php declare(strict_types = 1);
namespace Burrero\Shared\Infrastructure\Uuid4;

use Burrero\Shared\Domain\Uuid as Uuid4;
use Ramsey\Uuid\Uuid;

final class UuidApp implements Uuid4
{
    public function uuidCreate(): string
    {
        $this->uuid = Uuid::uuid4();
        return $this->uuid->toString();
    }
}
