<?php declare(strict_types=1);
namespace Burrero\Shared\Infrastructure\Core;

use Burrero\Shared\Domain\Core\Policy;
use Burrero\Shared\Infrastructure\Utils\BaseApplication;

class ApplicationPolicy extends BaseApplication implements Policy
{
    public function appCost(int $appSize, int $plan, int $storage, int $unit): float
    {
        $timeSto = 24;
        return ((($appSize * $timeSto)*$plan)/$storage)*$unit;
    }

    /**
     * Validate if ops is affordable
     * @param float $balance User balance
     * @param null|float $bill to execute
     * @param float $exCode Promotion code
     * @throw Exception 12 this execution is not affordable
     */
    public function itsAffordable(float $balance, ?float $bill, ?float $exCode): void
    {
        if ($bill != null) { 
            if ($balance < $bill) {
                throw new \Exception('#12#> this execution is not affordable!!!', 12);
            }
        }
    }

    public function appRef(string $params): mixed
    {
        //ref:589665+credit:10+via:paypal
        $params = explode('+',$params);
        return null;
    }
}
