<?php declare(strict_types=1);

namespace Burrero\Shared\Infrastructure\Core\Query;

use Psr\Log\LoggerInterface;
use Ramsey\Uuid\Uuid;

final class PhpEventQuery
{
    function __construct( 
        private LoggerInterface $loggerInterface
        )
    {
        
    }

    public function processNewEvent(string $id = null, array $event): bool
    {
        if ($id === null) {
            $id = Uuid::uuid4()->toString();
        }

        try {
            //$event[0]['name']
            $eventName = explode('@', $event[0]['name']);
            $setMethodToExecute = end($eventName);
            $this->loggerInterface->info(get_class() . '::class',[
                'id'        => $id,
                'Method'    => __METHOD__,
                'Throw'     => 'TouchDown!!!💞'
            ]);
            $newExecute = new $setMethodToExecute;
            $setBodyEventToArray = json_decode($event[0]['body']);
            foreach ($setBodyEventToArray as $key => $value) {
                call_user_func( $newExecute, $key, $value );
            }
            return true;

        } catch (\Throwable $th) {
            $this->loggerInterface->critical(get_class() . '::class',[
                'id'        => $id,
                'Method'    => __METHOD__,
                'Throw'     => $th->getMessage()
            ]);

            return false;
        }
        
    }

}
