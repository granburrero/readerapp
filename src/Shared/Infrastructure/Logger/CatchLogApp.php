<?php declare(strict_types = 1);
namespace Burrero\Shared\Infrastructure\Logger;

use Burrero\Shared\Domain\Logger;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;
use Dotenv\Dotenv;

final class CatchLogApp implements Logger
{
    public function catchString(string $app, string $message, array $context = []): self
    {
        $toJson = [
            "Time"=> self::dateOp(),
            "App"=> $app,
            "message"=>$message,
            "context"=>$context

        ];

        $this->json = json_encode($toJson);

        return $this;
    }

    public function sendCatchString(?int $mode = null): void
    {
        //mode null sent to .json
        switch ($mode) {
            case 1:

                //AMQP
                /*$dotenv = Dotenv::createImmutable(__DIR__.'/../../../../config', '.env.prod');
                $dotenv->load();
                $connection = new AMQPStreamConnection('shared_rabbitmq', 5672, $_ENV['RABBITUSER'], $_ENV['RABBITPASS']);
                $channel = $connection->channel();
                $channel->queue_declare('log-service', false, false, false, false);
                $msg = new AMQPMessage($this->json);
                $channel->basic_publish($msg,'','log-service');*/

                break;
            
            default:
            
                //service file .json
                if (!file_put_contents('app.log', $this->json)) {
                    echo" error in ".__METHOD__;
                }

                break;
        }
    }

    private static function dateOp(): string
    {
        $dateOn = date('d-M-Y H:i:s');
        return $dateOn;
    }
}