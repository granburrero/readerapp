<?php declare(strict_types=1);

namespace Burrero\Shared\Infrastructure\SecureDataControl;

use Burrero\Shared\Infrastructure\SecureDataControl\DataConst;

final class CheckSecureDataControl extends DataConst
{
    static function CheckData(string $userDir)
    {
        $array = [];
        $dataFile = glob(__DIR__.self::SDC_LOCATION.$userDir.'/*.php');

        foreach ($dataFile as $value) {
            include($value);

            if (self::checkDate($timeToDead) < 1) {
                $array[] = $value;
            }
        }
        for ($i=0; $i < count($array); $i++) { 
            unlink($array[$i]);
        }
    }

    static function CheckClientCacheSecureDataControl(string $clientUUID):bool
    {
        return self::checkSDCFile(self::SDC_LOCATION.$clientUUID, '/clientCache.php');
    }
}
//var_dump(CheckSecureDataControl::CheckData('3df3a951-5a6c-4889-93d2-f20435d11b22','9b2fd024-a937-42d2-86eb-677d9cb84df6'));