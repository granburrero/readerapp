<?php declare(strict_types=1);

namespace Burrero\Shared\Infrastructure\SecureDataControl;

use Burrero\Shared\Infrastructure\SecureDataControl\DataConst;
use Exception;

final class CreateSecureDataControl extends DataConst
{

    static function CreateClientCacheSecureDataControl($clientInstance, $clientUUID)
    {

        self::createSDCDir($clientUUID);

        $outputData = "<?php " .PHP_EOL;
        $outputData .= '$timeToCreate = ' . time() .";" . PHP_EOL;
        $outputData .= '$timeToDead = ' . time() + self::CLIENT_TIME .";" . PHP_EOL;
        $outputData .= '$dataCache = ';
        $outputData .= @var_export($clientInstance, true); 
        $outputData .= ";";

        $fileAction = file_put_contents(__DIR__.self::SDC_LOCATION.$clientUUID.'/clientCache.php', $outputData);
        $closeAction = chmod(__DIR__.self::SDC_LOCATION.$clientUUID, self::SDC_CLOSE_PERMISSION);

        if (is_numeric($fileAction) && $closeAction == true) {

            $dataSession = [
                'id'        => $clientUUID,
                'creted'    => date_create_from_format('Y-m-d h:i:s A', date('Y-m-d h:i:s A', time())),
                'expire'    => date_create_from_format('Y-m-d h:i:s A', date('Y-m-d h:i:s A', time() + self::CLIENT_TIME))
            ];

            return json_encode($dataSession);

        } else {
            throw new Exception('This action produce File Response: '.$fileAction.' and Secure Action: '.$closeAction, 4785);
        }

    }

    /**
     * @TODO all docs
     */
    static function CreateTransactionSecureDataControl(array $SDCData):string
    {
        //make a dir
        self::createSDCDir($SDCData['clientId']);

        $outputData = "<?php " .PHP_EOL;
        $outputData .= '$timeToCreate = ' .time() .";" . PHP_EOL;
        $outputData .= '$timeToDead = ' . time() + self::STANDAR_TIME .";" . PHP_EOL;
        $outputData .= '$dataCache = ';
        $outputData .= @var_export($SDCData,true); 
        $outputData .= ";";
            
        $fileAction = file_put_contents(__DIR__.self::SDC_LOCATION.$SDCData['clientId'].'/'.$SDCData['transactID'].'.php',$outputData);
        $closeAction = chmod(__DIR__.self::SDC_LOCATION.$SDCData['clientId'], self::SDC_CLOSE_PERMISSION);
        if (is_numeric($fileAction) && $closeAction == true) {
            $dataSession = [
                'id'        => $SDCData['clientId'].'+'.$SDCData['transactID'],
                'creted'    => date_create_from_format('Y-m-d h:i:s A', date('Y-m-d h:i:s A', time())),
                'expire'    => date_create_from_format('Y-m-d h:i:s A', date('Y-m-d h:i:s A', time() + self::STANDAR_TIME))
            ];

            return json_encode($dataSession);

        } else {
            throw new Exception('This action produce File Response: '.$fileAction.' and Secure Action: '.$closeAction, 4785);
        }
        
    }
}

/*$dataArray = [
    'clientId' => '3df3a951-5a6c-4889-93d2-f20435d11b22',
    'appNamespace' => 'Burrero.Backoffice.Application.DataTransform.PhpXlsxFileRead',
    'dependNamespace' => 'Burrero.Backoffice.Application.DataTransform.JsonToSqlTableVisitas',
    'appParams' => '[{"simpleID":1,"proId":"25","amount":2},{"simpleID":2,"proId":"31","amount":2},{"simpleID":3,"proId":"6","amount":1},{"simpleID":4,"proId":"25","amount":2},{"simpleID":5,"proId":"31","amount":2},{"simpleID":6,"proId":"6","amount":1},{"simpleID":7,"proId":"25","amount":2},{"simpleID":8,"proId":"31","amount":2},{"simpleID":9,"proId":"6","amount":1},{"coord":"10.686272202785565,-71.61005115433208"},{"dirRef":"calle-87-salvador-dali-85-47"}]',
    'appExecuted' => false,
    'clientBalance' => 19.997008,
    'transactID' => '0d8f8c7c-1abe-4b7e-a299-ec163a41c746',
    'appBill' => 0.000864,
    'appCallBill' => 'Burrero\\Backoffice\\Backend\\Controllers\\MainController',
    'transactType' => 'debit',
    'refTransact' => '9be23d4b-4548-4982-8b00-f04ef619c012'
];

$exc = new CreateSecureDataControl();
var_dump($exc->executeCreateSecureDataControl($dataArray));*/