<?php declare(strict_types = 1);
namespace Burrero\Shared\Infrastructure\SecureDataControl;

class DataConst
{
    const SDC_LOCATION = "/../../../../var/cache/SecureDataControl/";
    const SDC_OPEN_PERMISSION = 0775;
    const SDC_CLOSE_PERMISSION = 0644;
    const CLIENT_TIME = (7 * 24 * 60 * 60); //1 semana 604.800seg
    const STANDAR_TIME = (24 * 60 * 60); //24 horas 86.400seg

    /**
     * Create a directory from a client id if it does not exist, if it exists it assigns permissions for writing 
     * @param string $name UUID V4 type
     * @return void
     * @category folder handler
     * @access public
     * @static 
     */
    static function createSDCDir(string $name):void
    {
        if (self::checkSDCDir($name) == false) {
            mkdir(__DIR__.self::SDC_LOCATION.$name, self::SDC_OPEN_PERMISSION);
        } else {
            chmod(__DIR__.self::SDC_LOCATION.$name, self::SDC_OPEN_PERMISSION);
        }
    }

    /**
     * Check if a directory specified with the client id exists
     * @param string $name UUID V4 type
     * @return bool
     * @category folder handler
     * @access private
     * @static 
     */
    private static function checkSDCDir(string $name):bool
    {
        return is_dir(__DIR__.self::SDC_LOCATION.$name);
    }

    /**
     * Check if a filename exists 
     * @param string $dir UUID V4 type
     * @param string $file
     * @return bool
     * @category folder|file handler
     * @access public
     * @static 
     */
    static function checkSDCFile(string $dir, string $file):bool
    {
        return is_file(__DIR__.self::SDC_LOCATION.$dir.'/'.$file);
    }

    /**
     * Returns the absolute difference in seconds between 2 dates 
     * @param TimeStamp 
     * @return float
     * @category Data comparator
     * @access public
     * @static
     */
    static function checkDate(int $timeStampEnd):float
    {
        $end = date_create_from_format('Y-m-d h:i:s A', date('Y-m-d h:i:s A', $timeStampEnd));
        return self::s_datediff("s","now",$end, true);
    }

    private static function s_datediff( $str_interval, $dt_menor, $dt_maior, $relative=false){

        if( is_string($dt_menor)) $dt_menor = date_create( $dt_menor);
        if( is_string($dt_maior)) $dt_maior = date_create( $dt_maior);
 
        $diff = date_diff($dt_menor, $dt_maior, ! $relative);
       
        switch($str_interval){
            case "y":
                $total = $diff->y + $diff->m / 12 + $diff->d / 365.25; 
                break;
            case "m":
                $total= $diff->y * 12 + $diff->m + $diff->d/30 + $diff->h / 24;
                break;
            case "d":
                $total = $diff->y * 365.25 + $diff->m * 30 + $diff->d + $diff->h/24 + $diff->i / 60;
                break;
            case "h":
                $total = ($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h + $diff->i/60;
                break;
            case "i":
                $total = (($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i + $diff->s/60;
                break;
            case "s":
                $total = ((($diff->y * 365.25 + $diff->m * 30 + $diff->d) * 24 + $diff->h) * 60 + $diff->i)*60 + $diff->s;
                break;
           }
        if($diff->invert)
                return -1 * $total;
        else    return $total;
    }
}