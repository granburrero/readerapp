<?php declare(strict_types=1);

require_once __DIR__.'/bootstrap.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Ramsey\Uuid\Uuid;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Maknz\Slack\Client;

$appRouting = 'Container-Billing-Worker';
$log = new Logger($appRouting);
$log->pushHandler(new StreamHandler(__DIR__.'/var/logs/app.log', Logger::INFO));
$conn = $entityManager->getConnection();
$connection = new AMQPStreamConnection($_ENV['RABBIT_HOST'], 5672, $_ENV['RABBIT_USER'], $_ENV['RABBIT_PASS']);
$channel = $connection->channel();
$channel->queue_declare('billing-event', false, false, false, false);
$slackService = new Client($_ENV['SLACK_HOOK']);
$slackService->send('Burrero Billing Worker is UP!!!')->withIcon(':ojos_estrella:');
echo " [*] Waiting for messages. To exit press CTRL+C \n";

  $callback = function($msg) use ($log, $conn, $slackService) {
    $uuid = Uuid::uuid4();
    $id = $uuid->toString();
    

    echo ' [x] Received msg ',$id,"\n";

    $billingData = json_decode($msg->body, true);
    //$billingData = json_decode($sqlParameters['body'], true);//TODO PREPARE DATA 4 CREDIT AND DEBIT
    if ($billingData['transactType'] == "debit") {
      $ref= 'internalPre-charged';
    } else {
      $ref = $billingData['refTransact'];
    }
    $transacData = json_encode([
      "billId"        => $billingData['transactID'],
      "refTransact"   => $ref,
      "transactCode"  => $billingData['appCallBill'],
      "transactType"  => $billingData['transactType'],
      "amount"        => $billingData['appBill']
    ]);
    $billingValues = [
      'id'          => Uuid::uuid4()->toString(),
      'client'      => $billingData['clientId'],
      'transaction' => $transacData
    ];

    $sql ='INSERT INTO `burrero_transaction` (id, client, transaction) VALUES (:id, :client, :transaction)';

    if ($conn->executeQuery($sql, $billingValues)) {
        $log->info('DOCTRINE\\DBAL\\BILLING', [
          'id'                => $id,
          'eventToReproduce'  => $sql,
          'eventToResponce'   => $billingValues
        ]);
        if ($billingData['transactType'] == "credit") {
          $slackService->withBlock([
            'type'  => 'section',
            'text'  => 'New billing transact: '.$billingData['transactID']."\r\n Reference: ". $ref
          ])->withBlock([
            'type'  => 'section',
            'text'  => 'Billing Operation: '. $billingData['transactType'] ."\r\n  Amount:  ". $billingData['appBill']
          ])->send('New billing transact: '.$billingData['transactID']);
        }
      }
      
    };
    
    $channel->basic_consume('billing-event', '', false, true, false, false, $callback);
    
    while ($channel->is_open()) {
        $channel->wait();
    }