<?php
require_once __DIR__.'/kernel.php';
require_once __DIR__.'/bootstrap.php';
require_once __DIR__.'/libs/Spyc/Spyc.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Ramsey\Uuid\Uuid;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use Burrero\Shared\Infrastructure\Core\Query\PhpEventQuery;

$appRouting = 'Container-Main-Worker';
$log = new Logger($appRouting);
$log->pushHandler(new StreamHandler(__DIR__.'/var/logs/app.log', Logger::INFO));

$connection = new AMQPStreamConnection($_ENV['RABBIT_HOST'], 5672, $_ENV['RABBIT_USER'], $_ENV['RABBIT_PASS']);
$channel = $connection->channel();
$channel->queue_declare('container-main-work', false, false, false, false);

echo " [*] Waiting for messages. To exit press CTRL+C\n";

  $callback = function($msg) use ($log, $entityManager, $container){
    
    $uuid = Uuid::uuid4();
    $result = null;
    echo ' [x] Received ', $msg->body, "\n";

    sleep(10);

    $log->info('Batch created',[
      'id'      => $uuid->toString(),
      'event id' => $msg->body
    ]);
    
    //hotfix
    try {
      $query = $entityManager->getConnection();
      $arguments = [1 => $msg->body];
      $sql = 'SELECT * FROM `domain_events` WHERE aggregate_id = ?';
      $resultQuery = $query->fetchAllAssociative($sql, $arguments);
  
      if (!empty($resultQuery)) {
          $execute = new PhpEventQuery($container->get('Psr\\Log\\LoggerInterface'));
          $result = $execute->processNewEvent(null, $resultQuery);
      } else {

        $log->error('DOCTRINE\\DBAL',[
          'id'                => $uuid->toString(),
          'event id'          => $msg->body,
          'eventToReproduce'  => $sql,
          'eventArguments'    => $arguments,
          'eventToResponce'   => $result
        ]);
      
      }
  
    } catch (\Throwable $th) {
        echo 'RUNTIME\\FAIL'. $th->getMessage() ."\r\n";
    }
    //hotfix
  };
  
  $channel->basic_consume('container-main-work', '', false, true, false, false, $callback);
  
  while ($channel->is_open()) {
      $channel->wait();
  }