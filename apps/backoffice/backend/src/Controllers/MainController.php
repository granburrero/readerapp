<?php declare(strict_types=1);
namespace Burrero\Backoffice\Backend\Controllers;

require_once __DIR__.'/../../../../../kernel.php';

use Burrero\Shared\Domain\Core\Policy;
use Burrero\Shared\Domain\Messenger;
use Burrero\Shared\Infrastructure\Doctrine\BaseController;
use Burrero\Shared\Infrastructure\SecureDataControl\CreateSecureDataControl;
use Burrero\Shared\Infrastructure\SecureDataControl\CheckSecureDataControl;
use Burrero\Shared\Infrastructure\Utils\StringHandler;
use Psr\Log\LoggerInterface;

class MainController extends BaseController
{
    function __construct(
        private $x, 
        private LoggerInterface $logger, 
        private Messenger $messenger, 
        private Policy $policy
        )
    {
        global $argc, $argv;
        $this->countArguments = $argc;
        $this->argument = $argv;
        parent::__construct($this->x);
    }

    function mainMethod()
    {
        /**
         * $this->argument[0] = script
         * $this->argument[1] = string appId
         * $this->argument[2] = string clientId
         * $this->argument[3] = string params
         * max argument 4 $this->countArguments = 4
         */
         try {
            $eventId = $this->eventId();
            $client = $this->loadClient($this->argument[2]);
            $clientPlan = $this->loadClientPlan();
            $balance = $this->loadClientAccount()->getClientBalance();
            $appInstance = $this->loadApp($this->argument[1]);
            //////////////ParamsHandler///////////////
            $paramHandlers = new StringHandler;
            $paramsInstance = $paramHandlers->paramHandler($this->argument[3],$appInstance->getTypeTransaction());
            //////////////ParamsHandler///////////////
            if ($appInstance->appIsBillable() == true) {
                $bill = $this->policy->appCost(
                    8+16,
                    $clientPlan->getClientPlan(),
                    $clientPlan->getClientStorage(),
                    $clientPlan->getClientUnit()
                );
                $this->policy->itsAffordable($balance, $bill, null);
            } else {
                $bill = $paramsInstance->getAmountCredit();
            }
             ///////////////validations////////////////
             $this->offSetArguments();
             $client->isActive();
             $this->checkProductToUse($this->argument[1]);
             $appInstance->appIsActive();
             
             ///////////////validations////////////////
             
            $payload = [
                'clientId'          => $this->argument[2],
                'appNamespace'      => $appInstance->getNameSpace(),
                'dependNamespace'   => $appInstance->getDependNameSpace(),
                'appParams'         => $paramsInstance->getAllData(),
                'appExecuted'       => false,
                'clientBalance'     => $balance,
                'transactID'        => $eventId,
                'appBill'           => $bill,
                'appCallBill'       => get_class(),
                'transactType'      => $appInstance->getTypeTransaction(),
                'refTransact'       => $paramsInstance->getReference()
            ];

            $eventPackage = [
                'aggregate_id' => $eventId,
                'name'         => 'Burrero\Shared\Infrastructure\Core\ApplicationPolicy',
                'body'         => json_encode($payload)
            ];
            ///////////////////////////////////////////////////////////////////////
            //////////////////////////DATA SECURE CHECK////////////////////////////
            CheckSecureDataControl::CheckData($this->argument[2]);
            //////////////////////////DATA SECURE CHECK////////////////////////////
            ///////////////////////////////////////////////////////////////////////
            //////////////////////////DATA SECURE STORE////////////////////////////
            
            $toReturn = CreateSecureDataControl::CreateTransactionSecureDataControl($payload);
            $this->logger->info(get_class() . '::class',[
                'id'        => $eventId,
                'Method'    => __METHOD__,
                'Action'     => $toReturn
                ]);
                
            //////////////////////////DATA SECURE STORE////////////////////////////
            ///////////////////////////////////////////////////////////////////////
            
            $this->messenger->dispatch('domain-event', json_encode($eventPackage));
            $this->messenger->dispatch('billing-event', json_encode($payload));
            
            return [$toReturn];
         } catch (\Throwable $th) {
            
            $this->logger->critical(get_class() . '::class',[
                'id'        => $eventId,
                'Method'    => __METHOD__,
                'Throw'     => $th->getMessage()
                ]);
            return $th->getMessage();
         }
        
    }

    /**
     * @return void
     * @throw Exception 54
     */
    private function offSetArguments():void
    {
        if ($this->countArguments != 4) {
            throw new \Exception("Offset arguments,", 54);
        }
    }

} 
$exec = new MainController(
    $container->get('Doctrine\\ORM\\EntityManagerInterface'), 
    $container->get('Psr\\Log\\LoggerInterface'), 
    $container->get('Burrero\\Shared\\Infrastructure\\Messenger\\PhpRabbittMQMessageBus'), 
    $container->get('Burrero\\Shared\\Infrastructure\\Core\\ApplicationPolicy')
);
var_dump($exec->mainMethod());