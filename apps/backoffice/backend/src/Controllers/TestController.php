<?php declare(strict_types=1);
namespace Burrero\Backoffice\Backend\Controllers;

use Burrero\Shared\Infrastructure\Doctrine\BaseController;

require_once __DIR__.'/../../../../../kernel.php';

final class TestController extends BaseController
{
    function __construct(private $x, private $y)
    {
        parent::__construct($this->x, $this->y);
    }

    public function callClient($client): self
    {
        $this->loadClient($client);
        return $this;
    }

    public function payloadProcess(string $action, string $client)
    {
        /*$appArray = $this->loadApp($action);
        return [
            'app'=>$this->loadClass($appArray[0]['class']),
            'depends'=>$this->loadClass($appArray[1]['depends']),
            'client'=>$this->callClient($client)->clientToArray()
        ];*/

        $appArray = $this->loadApp($action);
        $app = $this->loadClass($appArray[0]['class']);
        $depend = $this->loadClass($appArray[1]['depends']);
        call_user_func(new $app, $client, $depend);
    }
}
$execute = new TestController($container->get('Doctrine\\ORM\\EntityManagerInterface'), $container->get('Burrero\\Shared\\Infrastructure\\Messenger\\PhpRabbittMQMessageBus'));
var_dump($execute->payloadProcess('09772ada-b724-4dda-8f3b-a141a9abdfcb','3df3a951-5a6c-4889-93d2-f20435d11b29'));
//php MainController.php 09772ada-b724-4dda-8f3b-a141a9abdfcb 3df3a951-5a6c-4889-93d2-f20435d11b29 proId:25*2+proId:31*2+proId:6*1+proId:25*2+proId:31*2+proId:6*1+proId:25*2+proId:31*2+proId:6*1+coord:10.686272202785565,-71.61005115433208+dirRef:calle-87-salvador-dali-85-47
//php MainController.php 843a8d2a-2242-441b-bf57-e04dfc9d59b2 3df3a951-5a6c-4889-93d2-f20435d11b29 ref:589665+credit:10+via:paypal