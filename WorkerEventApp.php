<?php declare(strict_types = 1);

require_once __DIR__.'/bootstrap.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Ramsey\Uuid\Uuid;
use PhpAmqpLib\Connection\AMQPStreamConnection;

$appRouting = 'Container-Event-Worker';
$log = new Logger($appRouting);
$log->pushHandler(new StreamHandler(__DIR__.'/var/logs/app.log', Logger::INFO));
$conn = $entityManager->getConnection();
$connection = new AMQPStreamConnection($_ENV['RABBIT_HOST'], 5672, $_ENV['RABBIT_USER'], $_ENV['RABBIT_PASS']);
$channel = $connection->channel();
$channel->queue_declare('domain-event', false, false, false, false);

echo " [*] Waiting for messages. To exit press CTRL+C\n";

  $callback = function($msg) use ($log, $conn) {
    $uuid = Uuid::uuid4();
    $id = $uuid->toString();

    echo ' [x] Received msg ',$id,"\n";
    
    $sqlParameters = json_decode($msg->body, true);
    $values = [
      'id'            => $id,
      'aggregate_id'  => $sqlParameters['aggregate_id'],
      'name'          => $sqlParameters['name'],
      'body'          => $sqlParameters['body']
    ];
    
    $sql ='INSERT INTO `domain_events` (id, aggregate_id, name, body) VALUES (:id, :aggregate_id, :name, :body)';

    if ($conn->executeQuery($sql,$values)) {
      $log->info('DOCTRINE\\DBAL\\EVENT_DOMAIN', [
        'id'                => $id,
        'eventToReproduce'  => $sql,
        'eventToResponce'   => $values
      ]);
    }
    
  };
  
  $channel->basic_consume('domain-event', '', false, true, false, false, $callback);
  
  while ($channel->is_open()) {
      $channel->wait();
  }