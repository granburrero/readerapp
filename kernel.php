<?php

use DI\ContainerBuilder;
use Dotenv\Dotenv;
require_once __DIR__.'/vendor/autoload.php';


$dotenv = Dotenv::createImmutable(__DIR__);
$dotenv->load();

date_default_timezone_set((empty($_ENV['TZ']) ? 'America/New_York' : $_ENV['TZ']));

$containerBuilder = new ContainerBuilder;

if ($_ENV['ENVO'] !== 'dev') {
    $containerBuilder->enableCompilation(__DIR__ . '/var/cache');
}

$containerBuilder->addDefinitions(__DIR__ . '/config/config.php');

$container = $containerBuilder->build();

return $container;