CREATE DATABASE IF NOT EXISTS `burrero`;
SET GLOBAL time_zone = '-4:00';
USE `burrero`;
CREATE TABLE IF NOT EXISTS `domain_events` (
  `id` CHAR(36) NOT NULL,
  `aggregate_id` CHAR(36) NOT NULL,
  `name` JSON NOT NULL,
  `body` JSON NOT NULL,
  `occurred_on` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE IF NOT EXISTS `burrero_clients` (
  `id` CHAR(36) NOT NULL,
  `name` VARCHAR(150) NOT NULL,
  `status` ENUM('active', 'inactive','suspended') NOT NULL,
  `products` JSON NOT NULL,
  `add_on` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE IF NOT EXISTS `burrero_app` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `client` CHAR(36) NOT NULL,
  `plan` INT(4) NOT NULL,
  `storage` INT(11) NOT NULL,
  `unit` INT(4) NOT NULL,
  `excode` VARCHAR(50) NULL,
  `occurred_on` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
CREATE TABLE IF NOT EXISTS `burrero_transaction` (
  `id` CHAR(36) NOT NULL,
  `client` CHAR(36) NOT NULL,
  `transaction` JSON NOT NULL,
  `occurred_on` timestamp NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
INSERT INTO `burrero_transaction` (`id`,`client`,`transaction`,`occurred_on`) VALUES ('d8f90fb0-6791-4c81-b883-9312a41e3318','3df3a951-5a6c-4889-93d2-f20435d11b29','{"billId":"25306cea-cf02-45aa-b12e-0d2712dcb98a","transactCode":"buy","transactType":"credit","amount":5}',NOW());
INSERT INTO `burrero_transaction` (`id`,`client`,`transaction`,`occurred_on`) VALUES ('68cb2c2e-2d38-49fe-82f5-3b6436e9aab0','3df3a951-5a6c-4889-93d2-f20435d11b29','{"billId":"b3e622ae-d7c9-4087-893f-beb662e8313b","transactCode":"Install","transactType":"debit","amount":0.0004}',NOW());
INSERT INTO `burrero_app` (`client`,`plan`,`storage`,`unit`,`excode`,`occurred_on`) VALUES ('3df3a951-5a6c-4889-93d2-f20435d11b29', 5, 1000000000, 300, NULL, NOW());
INSERT INTO `burrero_clients` (`id`, `name`, `status`, `products`, `add_on`) 
SELECT * FROM (SELECT '3df3a951-5a6c-4889-93d2-f20435d11b29' AS `id`, 'CristalMedia LLC' AS `name`, 'active' AS `status`, '{"09772ada-b724-4dda-8f3b-a141a9abdfcb":0}' AS `products`, NOW() AS `add_on`) AS temp
WHERE NOT EXISTS (
  SELECT `id` FROM `burrero_clients` WHERE `id` = '3df3a951-5a6c-4889-93d2-f20435d11b29'
  ) LIMIT 1;